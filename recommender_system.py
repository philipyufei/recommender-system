import controller


def main():
    """
    This main function just call functions in different files. It mainly did the
    following things:
        1. Read the dataset.
        2. Get num_ratings, num_users, num_items of the dataset.
        3. Construct a rating matrix (rating_set).
        4. Construct 10% traing/testing set based on rating_set.
        5. Calulate the cosine similarities of the traing set.
        6. Use the cosine similarities to calculte root mean square error with
             knn, where k = [5, 25, 50, 100, 200, All]
        7. Plot the result as a bar chart.
    """
    dataset = controller.get_data_set('u.data')
    
    num_entries, num_users, num_items = controller.get_dataset_info(dataset)
    
    rating_set = controller.get_rating_set(dataset, num_users, num_items)
   
    training_set, testing_set = controller.get_training_testing_set(
        rating_set, num_entries, num_users, num_items)
    
    similarities = controller.get_similarities(training_set)
    
    top_ks, mses = controller.get_rmses(similarities, training_set, testing_set)
    
    controller.plot_result(top_ks, mses)


if __name__ == '__main__':
    main()
