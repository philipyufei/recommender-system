import math
import numpy as np
import pandas as pd
from copy import deepcopy as dc
from sklearn.metrics.pairwise import cosine_similarity


def get_data_set(filename):
    """
    Load dataset (u.data) where the full contains:
        100000 ratings by 943 users on 1682 items.
    Each line of data is a tab separated list of  
        user id | item id | rating | timestamp.
    The time stamps are unix seconds since 1/1/1970 UTC 
    Returns: 
        dataset
    """
    return pd.read_csv(filename, skiprows=0, sep='\t', 
        names=['user_id', 'item_id', 'rating', 'timestamp'])


def get_dataset_info(dataset):
    """
    The dataset u.data where the full contains:
        100000 ratings by 943 users on 1682 items.
    Args:
        detaset: the dataset u.data read by pandas
    Returns: 
        Three integers
        num_entries, num_users, num_items
    """
    return len(dataset), max(dataset.user_id), max(dataset.item_id)


def get_rating_set(dataset, rows, cols):
    """
    Each entry is: 
        Pandas(Index=0, user_id=196, item_id=242, rating=3, timestamp=881250949)
    Args:
        detaset: the dataset u.data read by pandas
        rows: number of user_ids (user_ids start from 1)
        cols: number of item_ids (item_ids start from 1)
    Returns:
        A numpy 2D arrays with 'rows' rows and 'cols' columns 
        rating_set
    """
    rating_set = np.zeros((rows, cols))
    for entry in dataset.itertuples():
        rating_set[entry[1] - 1, entry[2] - 1] = entry[3]
    print("Rating Set is generated.")
    return rating_set


def get_training_testing_set(rating_set, entries, rows, cols):
    """
    Data process: split training and test data set:
        We use 90% training - 25% testing cross validation
    Args:
        rating_set: the
        entries: num_entries     -> 100000
        rows: number of user_ids -> 943
        cols: number of item_ids -> 1682
    Returns:
        Two numpy 2D arrays with 'rows' rows and 'cols' columns 
        training_set, test_set
    """
    # Calculate how may ratings we are used for testing for each user
    test_num = math.floor(0.1 * entries / rows)
    
    # initialize training and testing set
    training_set, testing_set = dc(rating_set), np.zeros((rows, cols))
    
    # update it testing set for a random choice
    for u_id in range(rows):
        i_id = np.random.choice(
            rating_set[u_id, :].nonzero()[0], size=test_num, replace=False)
        testing_set[u_id, i_id] = rating_set[u_id, i_id]
    print("Training Set and Testing Set are generated.")

    return training_set - testing_set, testing_set


def get_similarities(matrix):
    """
    Calculate cosine similarity provided by sklearn.metrics.pairwise
    url = "http://scikit-learn.org/stable/modules/generated/
            sklearn.metrics.pairwise.cosine_similarity.html"
    Args:
        matrix: the input set, here should be training set
    Returns:
        A square matrix of len(matrix) rows and colemns
        similarities
    """
    print("Cosine Similarities is generated.\n")
    return cosine_similarity(matrix)
    