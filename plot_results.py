import matplotlib.pyplot as plt
import numpy as np


def plot_all(top_ks, mses):
    """
    Plot the result as a bar chart.
    Args:
        top_ks: A list of ks we used to calculate the mse.
        mses: A list of mse for the corresponding k.
    Returns:
        None
    """
    # Get the width of each bar
    x_pos = np.arange(len(top_ks))

    # Initialize a graph
    fig, graph = plt.subplots()

    # Draw the bar chart
    chart = graph.bar(x_pos, mses, align='center', alpha=0.5)

    # Set tile, x_label, y_label for the bar chart
    graph.set_title('RMSE of Rating Data and Testing Data via K Nearest Neighbours')
    graph.set_xlabel('K Nearest Neighbours')
    graph.set_ylabel('RMSE')
    graph.set_xticks(x_pos)
    graph.set_xticklabels(top_ks)

    # Set the mse value for each bar
    i = 0
    for bar in chart:
        height = bar.get_height()
        graph.text(bar.get_x() + bar.get_width() / 2, 1.01 * height,
         str(mses[i]),  ha='center', va='bottom', fontweight='bold')
        i += 1
    
    # show the bar chart
    plt.show()
    