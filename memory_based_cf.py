import numpy as np


def calculate_rmse(testing_set, predict_set):
    """
    Calculate the mean square error of two sets.
        MSE = average((correct value - predict value) ** 2)
    Args:
        testing_set: a set that contains the target values.
        predict_set: a set that contains the values we recommended.
    Returns:
        rmse: The root mean square error of two sets.
    """
    correct_value = testing_set[testing_set.nonzero()].flatten()
    predict_value = predict_set[testing_set.nonzero()].flatten()
    return np.sqrt(np.average((correct_value - predict_value) ** 2))


def get_predict(uid, iid, similarities, knn, training_set):
    """
    Calulate the predict value for an item given a user.
    Agrs: 
        uid: The user id.
        iid: The item id.
        similarities: The cosine similarities of the rating set.
        knn: A list of k nearest neighbour's user ids.
        training_set: A set that record the missing data for testing.
    Returns:
        predict: The predict value calculate via weighted average.
    """
    simils_ui, rating_ui = similarities[uid,:][knn],training_set[:,iid][knn]
    rating_ui_bar = (np.sum(rating_ui) / len(rating_ui))
    right_part = simils_ui.dot(rating_ui - rating_ui_bar) / np.sum(simils_ui)
    return rating_ui_bar + right_part


def get_knn(uid, k, similarities):
    """
    Get the k nearest neighbours for a given user who has id uid.
    Args:
        uid: the id of the target user.
        k: the value used in knn, to find the k's nearest neighbour for a user.
        similarities: The cosine similarities of the rating set.
    Returns:
        knn: A list of k nearest neighbour's user ids.
    """
    return [np.argsort(similarities[:, uid])[-2: -k - 2: -1]]


def get_rmse(k, similarities, training_set, testing_set):
    """
    Calculate the mse for a specific top-k.
    Args:
        k: The top-k value using for knn.
        similarities: The cosine similarities of the rating set.
        training_set: A copy of rating set where some of the data are missing.
        testing_set: A set that record the missing data for testing.
    Returns:
        rmse: The root mean square error of traing_set and predict_set at top-k.
    """
    rows, cols = training_set.shape
    predict_set = np.zeros((rows, cols))

    for uid in range(rows):
        knn = get_knn(uid, k, similarities)
        for iid in range(cols):
            predict_set[uid, iid] = \
                get_predict(uid, iid, similarities, knn, training_set)
            
    return calculate_rmse(testing_set, predict_set)
    

def get_rmses(similarities, training_set, testing_set):
    """
    Calculate the MSE for a several top-ks and all of the users.
    Args:
        similarities: The cosine similarities of the rating set.
        training_set: A copy of rating set where some of the data are missing.
        testing_set: A set that record the missing data for testing.
    Returns:
        top_ks: A list of top-k values.
        rmses: A list of root mean square errors of traing_set and predict_set.
    """
    rmses, top_ks = [], [10, 25, 50, 100, 200, len(similarities)]
    for k in top_ks:
        print("Calculating MSE for top", k, "similar users ...")
        rmse = get_rmse(k, similarities, training_set, testing_set)
        rmses.append(rmse)
        if k != len(similarities):
            print('MSE of predicts for top', k, '=', rmse, '\n')
        else:
            print('MSE of predicts for all uses =', rmse, '\n')
    return top_ks, rmses
