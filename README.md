# Recommender System Using Collaborative Filtering

A recommender system or a recommendation system (sometimes replacing "system" with a synonym such as platform or engine) is a subclass of information filtering system that seeks to predict the "rating" or "preference" a user would give to an item.

Recommender systems have become increasingly popular in recent years, and are utilized in a variety of areas including movies, music, news, books, research articles, search queries, social tags, and products in general. There are also recommender systems for experts, collaborators, jokes, restaurants, garments, financial services, life insurance, romantic partners, and Twitter pages.

Collaborative filtering (CF) is a technique used by recommender systems. Collaborative filtering has two senses, a narrow one and a more general one. 

In this project, we used a memory-based CF with user-based similatity to predict on users� movie ratings.


## Getting Started

This project consists:

* README.md: It is a github readme style file, but you also can read it offline.
* controller.py: a link of main function (recommender_system.py) to different modals
* memory_based_cf.py: Contains the core function to calculate predicts and check the MSEs compair to the rating set.
* plot_results.py: Plot the final result as a bar chart.
* preparation.py: Read the dataset, construct rating_set, trainging_set, testing_set; and calculate user-based cosine similatity.
* recommender_system.py: The main function of the project.
* u.data: The dataset we chose for the project.


## Prerequisites and Installations

Before running this project, you need to install the following things on your machine:

1. Python3.6
	
	You can download and install python3.6 throght [here](https://www.python.org/downloads/)
	
2. pandas

	To install pandas on your local machine, you can simply use:
	
	```
	sudo python3 -m pip install pandas
	```

3. numpy

	Similar to install pandas, you can simply use:
	
	```
	sudo python3 -m pip install numpy
	```
	
4. matplotlib
	
	Again, the instalation method is similar to install numpy and pandas:
	
	```
	sudo python3 -m pip install matplotlib
	```

5. sklearn

	This library is really important and you can install is simply by:

	```
	sudo python3 -m pip install sklearn
	```

	
6. pip3 (Just in case you do not have it)

	If you are using Mac, you can install pip by
	
	```
	 python3 get-pip.py
	```
	
	If you are using Linux, you can install pip by
	
	```
	sudo apt-get install python3-pip
	```
	 
	
## Run the Project 

Once you have installed all of the installations, you can run the project as follows in terminal.

1. Run the project by typing the following command in terminal:

	```
	python3 recommender_system.py
	```

2. Waiting for processing

	After you run the project, you can see some feedbacks

## Built With

* [Python 3.6](https://www.python.org/downloads/) - Build and run the project
* [numpy](http://www.numpy.org/) - Required library for calculating
* [matplotlib](https://matplotlib.org/downloads.html) - Required library for ploting
* [pandas](https://pandas.pydata.org/) - Required library for loading .csv dataset file
* [sklearn](http://scikit-learn.org/stable/index.html) - calculating cosine similarities

## Authors

* **Yufei Zhang** - *z5121128*
* **Fan Xia** - *z5084401*

## Documentations

You can check our report for more details.

## License

This project is licensed under the GNU GPL License - see the [LICENSE](https://bitbucket.org/philipyufei/recommendersystem/src/master/LICENSE) file for details

## Acknowledgments

* [Google Python Style Guide](https://google.github.io/styleguide/pyguide.html)
* [Dataset Readme](http://files.grouplens.org/datasets/movielens/ml-100k-README.txt)
* [Dataset ml-100k](http://files.grouplens.org/datasets/movielens/ml-100k.zip)
