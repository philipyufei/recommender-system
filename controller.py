import preparation
import memory_based_cf
import plot_results


def get_data_set(filename):
    return preparation.get_data_set(filename)


def get_dataset_info(dataset):
    return preparation.get_dataset_info(dataset)


def get_rating_set(dataset, rows, cols):
    return preparation.get_rating_set(dataset, rows, cols)


def get_training_testing_set(rating_set, entries, rows, cols):
    return preparation.get_training_testing_set(rating_set, entries, rows, cols)


def get_similarities(matrix):
    return preparation.get_similarities(matrix)


def get_rmses(similarities, training_set, testing_set):
    return memory_based_cf.get_rmses(similarities, training_set, testing_set)


def plot_result(top_ks, mses):
    return plot_results.plot_all(top_ks, mses)
